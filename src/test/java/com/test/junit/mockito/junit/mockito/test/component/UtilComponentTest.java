package com.test.junit.mockito.junit.mockito.test.component;

import com.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.test.junit.mockito.junit.mockito.test.properties.ApplicationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UtilComponentTest {
  
  @InjectMocks
  private UtilComponent utilComponent;
  
  @Mock
  private ApplicationProperties applicationProperties;
  
  private PersonEntity personEntity;
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    
    personEntity = new PersonEntity();
    personEntity.setPersonId(4);
    personEntity.setName("Daniel");
    personEntity.setLastName("Avila");
    personEntity.setBirthDate("11/11/1995");
    personEntity.setStatus("00");
  }
  
  @Test
  public void mapperTest() {
    Mockito.when(applicationProperties
        .getPostulanteReason()).thenReturn("Sin estus");    
    utilComponent.mapper(personEntity);
  }
  
  
  
  
}
